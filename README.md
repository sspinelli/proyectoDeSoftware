# Proyecto de Software 2016
<br>
En este repositorio se encuentran ejercicios prácticos resueltos de la materia **Proyecto de Software**.
Cada **práctica** tiene un :file_folder: para sus ejercicios.
<br><br>
## Prácticas 
* [Práctica 1](https://gitlab.com/saczuac/proyectoDeSoftware/tree/master/P1) :white_check_mark:
* [Práctica 2](https://gitlab.com/saczuac/proyectoDeSoftware/tree/master/P2) :white_check_mark:
* [Práctica 3](https://gitlab.com/saczuac/proyectoDeSoftware/tree/master/P3) :white_check_mark:
<br>

### Tecnologías utilizadas en la materia <hr>

+ `PHP`
+ `HTML5`
+ `CSS3`
+ `JavaScript`
+ `Twig`
